#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <unistd.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <netdb.h> 


void main()
{

// getting cpu info
int cpu_line = 5;
char c[256],cp[256];
  int line_count_cpu=1, i=0, j=0;
  FILE *file;
  file=fopen("/proc/cpuinfo", "r");
  //getting the cpu info from the file particularly 5th line in a charecter array
  while(line_count_cpu <= cpu_line)
  {
    fgets(c, sizeof(c), file);
    if(line_count_cpu == cpu_line)
        break;
    line_count_cpu++;
  }
   //removing white spaces and tabs and new line charecters from the string
   while (c[i] != '\0')
   {
      if ((c[i] != ' ')&&(c[i] != '\t')&&(c[i] != '\n')) {
        cp[j] = c[i];
        j++;
      }
      i++;
   }
   i=0;j=0;
 
  char *cpu_detail = cp;
     //printf("%s", cpu_detail);

  

//getting ram size
 char r[256], rm[256];
 int line_count_ram=1, ram_line=1;
  FILE *file2;
  file2=fopen("/proc/meminfo", "r");
  //getting ram info from the file particularly from the 1st line in a charecter array
  while(line_count_ram <= ram_line)
  {
    fgets(r, sizeof(r), file2);
    if(line_count_ram == ram_line)
      break;
    line_count_ram++;
  }
  //removing the white spaces, tabs and new line charecters from the string
   while (r[i] != '\0')
   {
      if ((r[i] != ' ')&&(r[i] != '\t')&&(r[i] != '\n')) {
        rm[j] = r[i];
        j++;
      }
      i++;
   } 
   i=0;j=0;

//getting username
char bash_cmd[20] = "whoami";
char str[20], uname[20];
FILE *pipe;
int len; 

pipe = popen(bash_cmd, "r");

//getting the output of command and copying into a charecter array
fgets(str, sizeof(str), pipe);

len = strlen(bash_cmd);
bash_cmd[len-1] = '\0'; 
//removing white spaces and tabs and new line charecters from the string
while (str[i] != '\0')
   {
      if ((str[i] != ' ')&&(str[i] != '\t')&&(str[i] != '\n')) {
        uname[j] = str[i];
        j++;
      }
      i++;
   }
pclose(pipe);
i=0;j=0;

//getting OS and its version
char operating_system[256] = "grep 'PRETTY_NAME' /etc/*release";
char str1[256], os_name[256];
FILE *pipe2;
int len1;

pipe2 = popen(operating_system, "r");
//getting the output of command and copying into charecter array
fgets(str1, sizeof(str1), pipe2);
len1 = strlen(operating_system);
operating_system[len-1] = '\0';
//removing white spaces and tabs and newline charecter
while(str1[i] != '\0')
  {
   if ((str1[i] != ' ')&&(str1[i] != '\t')&&(str1[i] != '\n')) { 
      os_name[j] = str1[i];
      j++;
   }
   i++;
}
pclose(pipe2);


//posting the values to the server
 int portno = 9000; //update with port number where the server is running
    char *host = "localhost"; //update the IP address
    char *message_fmt = "POST /?uname=%s&cpu=%s&ram=%s&os=%s HTTP/1.0\r\n\r\n"; //format of url to the server with parameters
    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total;
    char message[8000],response[4096];
    sprintf(message,message_fmt,uname,cp,rm,os_name); //passing the value to the url format

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("Sorry, Run again");

    server = gethostbyname(host);
    if (server == NULL) printf("Sorry, Run again");

    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        error("Sorry, Run again");

    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd,message+sent,total-sent);
        if (bytes < 0)
            error("Sorry, run again");
        if (bytes == 0)
            break;
        sent+=bytes;
    } while (sent < total);

    
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
    do {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
            error("Sorry, check internet connectivity");
        if (bytes == 0)
            break;
        received+=bytes;
    } while (received < total);

    if (received == total)
        error("Check internet connectivity");

    /* close the socket */
   close(sockfd);

    /* process response */
    //printf("Response:\n%s\n",response);
    printf("Hello world");


//closing the files 

fclose(file);
fclose(file2);


}
