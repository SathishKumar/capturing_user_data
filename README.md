## Welcome

This is a simple program to give a demo while explaining the **Free Software Philosophy**. The c program code will take the **user details** such as **User name, os name and its version, ram size, cpu processor name** and send to my python server (*app.py*). The python program app.py will capture that detais. Finally the c program just print **Hello world** nothing else. **This only works with GNU/Linux Operating System**. 

### Steps

#### C Program

1. Open system_details.c 
2. edit the IP address with where the app.py is running (will see in configuring the app.py section) in the **line no 117** 
3. change the port no in the **line 116**
4. save the program 
5. compile the program **gcc filename.c -o some_name**
6. distribute the *binary file alone to the audience 
7. ask them to change the binary file to executable mode using the command ```chmod +x filename```
8. make them to run using ```./filename``` and find the output.

#### app.py program

1. Open the app.py
2. edit the value for host with your IP address (same must be given to the c program as mentioned in 2 point of about section)
3. edit the port with the port number in which you want to run the program (same must be given to the c program as mentioned in 3 point of about section)
4. run app.py

#### requirements

1. flask - micro web framework of python 
2. basic knowledge on python flask

for more details, contact: sathishsathi500@gmail.com, 7502273418.

#### Output to the user

Output will be **Hello world** nothing else.

#### Output to us

Users system infromations mentioned above

#### Objective

**Only to explain the consequences of closed source**

#### **Note:**

Datas does not stored in the databases. 


### **Licensed under [GPLV3](http://gplv3.fsf.org/)**

## **Thank You**
