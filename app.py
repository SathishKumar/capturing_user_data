from flask import Flask, Response, request, render_template


app = Flask(__name__)

@app.route('/', methods=["GET", "POST"])
def home():
    if request.method == "GET":
        return render_template("form.html")
    if request.method == "POST":
        name = request.args.get('uname')
        cpu = request.args.get('cpu')
        ram = request.args.get('ram')
        print(name)
        print(cpu)
        print(ram)
        return ("Done")

app.run(host="192.168.1.173", port=80)
